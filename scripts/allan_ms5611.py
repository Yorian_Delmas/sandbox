import numpy as np
import serial
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import matplotlib
from rr_controls_simulator_common.allan import overlap_allan_dev
from datetime import datetime
import sys

class baroHistogram:

	groundBaro = None
	airBaro = None
	str1 = None
	str2 = None
	groundData = []
	airData = []
	groundAvg = 0


	def openSerial(self):
		self.groundBaro = serial.Serial('/dev/ttyUSB1', 115200)
		self.airBaro = serial.Serial('/dev/ttyUSB0', 115200)

	def readBaro(self):
		self.str1 = self.groundBaro.readline().rstrip() # read data from serial 
		self.str2 = self.airBaro.readline().rstrip() # read data from serial

	def toFloat(self):
		split1 = self.str1.split(".")
		split2 = self.str2.split(".")
		try:
			self.groundData.append(float(split1[0]) + float(split1[1])/pow(10,len(split1[1])))
			self.airData.append(float(split2[0]) + float(split2[1])/pow(10,len(split2[1])))
		except:
			print "serial error"

	def allanDeviation(self):
		delta  = []

		for x in range(0,len(self.groundData)):
			delta.append(self.groundData[x] - self.airData[x])

		data = np.asarray(delta)
		taus, adev = overlap_allan_dev(data, 1)
		np.savetxt("pressure-%s.delta" %(datetime.now()),delta,fmt='%10.5f')
		plt.loglog(taus, adev ,'.', label='stuff')
		plt.xlabel('Tau')
		plt.ylabel('Adev')
		plt.title('Allan deviation, Fs = 100Hz')
		plt.show()

bh = baroHistogram()
bh.openSerial()

nbSp = int(sys.argv[1])
if nbSp < 100:
	nbSp = 100

while len(bh.groundData) < nbSp :
	now = str(datetime.now())
	bh.readBaro()
	bh.toFloat()
	print len(bh.groundData)

bh.allanDeviation()


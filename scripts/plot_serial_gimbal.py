import matplotlib.pyplot as plt
import serial
import sys
import numpy as np

ser = serial.Serial("/dev/ttyUSB0", 115200)

tA = []
tB = []
tC = []
phase = []

plt.ion()

x = np.arange(0, 10, 0.1)

fig = plt.figure()
ax1 = fig.add_subplot(111)
# ax1.set_ylim([-3.14,3.14])
# ax1.set_ylabel('Pitch [rad/sec]', color ='b')
ax1.set_ylim([-1,1])
ax1.set_ylabel('tA', color ='b')

for tl in ax1.get_yticklabels():
    tl.set_color('b')

ax2 = ax1.twinx()
ax2.set_ylim([-1,1])
ax2.set_ylabel('tB', color = 'r')
# ax2.set_ylim([-90,90])
# ax2.set_ylabel('Pitch [deg]', color ='r')
for tl in ax2.get_yticklabels():
    tl.set_color('r')

ax3 = ax1.twinx()
ax3.set_ylim([-1,1])
ax3.set_ylabel('tC', color = 'g')
# ax3.set_ylim([-90,90])
# ax3.set_ylabel('Pitch [deg]', color ='r')
for tl in ax3.get_yticklabels():
    tl.set_color('r')

# ax4 = ax1.twinx()
# ax4.set_ylim([-1,1])
# ax4.set_ylabel('phase [deg]', color = 'm')
# # ax4.set_ylim([-90,90])
# # ax4.set_ylabel('Pitch [deg]', color ='r')
# for tl in ax4.get_yticklabels():
#     tl.set_color('r')

# plt.title("Kp pos = 0 ; Ki_pos = 0; Kp_vel = 20 ; Ki_vel = 0 ; F = 20Hz")
ax1.set_xlabel('Phase (deg)')

for i in range(200):
	data = ser.readline().split(";")

	try:
		# torques.append(float(torque))
		# pitchs.append(float(pitch))
		# print len(torques)
		# print pitchs[-1]
		# print torques[-1]
		tA.append(float(data[0]))
		tB.append(float(data[1]))
		tC.append(float(data[2]))
		phase.append(float(data[3]))
		print len(tA)
	except Exception, e:
		pass

plt.show()
plt.draw()

ax1.plot(phase, tA, color = 'b')
ax2.plot(phase, tB, color = 'r')
ax3.plot(phase, tC, color = 'g')
# ax4.plot(phase, color = 'm')

plt.pause(2**31-1)


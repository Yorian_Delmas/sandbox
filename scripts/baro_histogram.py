import numpy as np
import serial
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import matplotlib
from rr_controls_simulator_common.allan import overlap_allan_dev
from datetime import datetime
import sys

class baroHistogram:

	groundBaro = None
	airBaro = None
	str1 = None
	str2 = None
	groundData = []
	airData = []
	groundAvg = 0


	def openSerial(self):
		self.groundBaro = serial.Serial('/dev/ttyUSB1', 115200)
		self.airBaro = serial.Serial('/dev/ttyUSB0', 115200)

	def readBaro(self):
		self.str1 = self.groundBaro.readline().rstrip() # read data from serial 
		self.str2 = self.airBaro.readline().rstrip() # read data from serial

	def toFloat(self):
		split1 = self.str1.split(".")
		split2 = self.str2.split(".")
		try:
			self.groundData.append(float(split1[0]) + float(split1[1])/pow(10,len(split1[1])))
			self.airData.append(float(split2[0]) + float(split2[1])/pow(10,len(split2[1])))
		except:
			print "serial error"

	def histogram(self):
		n, bins, patches = plt.hist(self.groundData, len(self.groundData), normed=1, facecolor='green', alpha=0.5)
		plt.xlabel('Pressure')
		plt.ylabel('Probability')
		plt.title(r'Histogram of baro pressure:')
		plt.axvline(self.groundAvg, color='b', linestyle='dashed', linewidth=2)
		plt.subplots_adjust(left=0.15)
		plt.show()

	def avg(self):
		sum = 0
		for elm in self.groundData:
			sum += elm
		self.groundAvg = sum/(len(self.groundData)*1.0)

bh = baroHistogram()
bh.openSerial()



nbSp = int(sys.argv[1])
if nbSp < 100:
	nbSp = 100

while len(bh.groundData) < nbSp :
	now = str(datetime.now())
	bh.readBaro()
	bh.toFloat()
	print len(bh.groundData)

bh.histogram()
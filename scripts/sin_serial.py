import serial
import numpy as np
from matplotlib import pyplot as plt
ser = serial.Serial('/dev/ttyUSB0', 115200)

serial_line = None
serial_split = None
sin1 = 0
sin2 = 0
sin3 = 0

def SerialSplit():
    serial_line = ser.readline().rstrip()
    serial_split = serial_line.split("\t")

def strToFloat():
    split1 = serial_split[0].split(".")
    sin1 = float(split1[0]) + float(split1[1])/pow(10,len(split1[1]))

    split2 = serial_split[1].split(".")
    sin2 = float(split2[0]) + float(split2[1])/pow(10,len(split2[1]))

    split3 = serial_split[2].split(".")
    sin3 = float(split3[0]) + float(split3[1])/pow(10,len(split3[1]))


plt.ion() # set plot to animated
 
ydata = [0] * 50
ax1=plt.axes()  
 
# make plot
line, = plt.plot(ydata)
plt.ylim([10,40])
 
while sin1 == 0:
    SerialSplit()
    print serial_line

print type(serial_split[0])
print serial_split

strToFloat()

print type(sin1)
print sin1
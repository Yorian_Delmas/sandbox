import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as lines
import sys
import math 
from time import sleep

alpha = None
beta = None
sqrt3 = 1.73205080757
u = None
v = None
w = None
ur = 0
ul = -100


angle = float(sys.argv[1])
rad_angle = angle*math.pi/180
alpha = math.cos(rad_angle)
beta = math.sin(rad_angle)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
plt.axis('equal')
plt.ylim([-1,1])
plt.xlim([-1,1])
ax.plot([0, +1], [0, 0], color='k', linestyle='-', linewidth=1)
ax.plot([0, 0.5], [0, 0.86602540378], color='k', linestyle='-', linewidth=1)
ax.plot([0, -0.5], [0, -0.86602540378], color='k', linestyle='-', linewidth=1)
ax.plot([0, -1], [0, 0], color='k', linestyle='-', linewidth=1)
ax.plot([0, -0.5], [0, +0.86602540378], color='k', linestyle='-', linewidth=1)
ax.plot([0, 0.5], [0, -0.86602540378], color='k', linestyle='-', linewidth=1)

ax.plot([0, alpha], [0, beta], color='r', linestyle='-', linewidth=2)

circ = plt.Circle((0, 0), radius=1, edgecolor='k', facecolor='None')
ax.add_patch(circ)


def updatePlot():
	ax.plot([0, +1*abs(u)], [0, 0], color='g', linestyle='-', linewidth=2)
	ax.plot([0, -0.5*abs(v)], [0, -0.86602540378*abs(v)], color='g', linestyle='-', linewidth=2)
	ax.plot([0, -0.5*abs(w)], [0, +0.86602540378*abs(w)], color='g', linestyle='-', linewidth=2)

	ax.plot([0, alpha], [0, beta], color='r', linestyle='-', linewidth=2)
	plt.show()

def SVM():

	if beta > 0.0 :
		# =====
		# Q1
		# =====
		if alpha > 0.0 : 
			# =====
			# Q1 S2
			# =====
			if sqrt3 * beta > alpha : 
				ur = alpha + 1/sqrt3 * beta
				ul = -alpha  + 1/sqrt3 * beta
			# =====
			# Q1 S1
			# =====	
			else : 
				ur = alpha - 1/sqrt3 * beta
				ul = 2 / sqrt3 * beta 
		# =====
		# Q2
		# =====		
		else :
			# =====
			# Q2 S3
			# =====
			if -sqrt3 * beta > alpha : 
				ur = 2/sqrt3 * beta
				ul = alpha - 1/sqrt3 * beta
			# =====
			# Q2 S2
			# =====
			else :
				ur = -alpha + 1/sqrt3 * beta
				ul = alpha + 1/sqrt3 * beta

	else : 
		# =====
		# Q4
		# =====
		if alpha > 0.0 :
			# =====
			# Q4 S5
			# =====
			if -sqrt3 * beta > alpha :
				ur = -alpha + 1/sqrt3 * beta
				ul = alpha + 1/sqrt3 * beta
			# =====
			# Q4 S6
			# =====
			else :
				ur = 2/sqrt3 * beta
				ul = alpha + 1/sqrt3 * beta			
		# =====
		# Q3
		# =====
		else : 
			# =====
			# Q3 S4
			# =====
			if sqrt3 * beta > alpha :
				ur = alpha  - 1/sqrt3 * beta
				ul = 2/sqrt3 * beta
			# =====
			# Q3 S5
			# =====
			else :
				ur = alpha + 1/sqrt3 * beta
				ul = -alpha + 1/sqrt3 * beta

print "angle %f alpha %f beta %f " %(rad_angle, alpha, beta)
SVM()
print "ur %f ul %f" %(ur,ul)

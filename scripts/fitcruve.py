import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import numpy as np

zoomtab = np.array([
0x8400,
0x8EC0,
0x9400,
0x9740,
0x9960,
0x9AE0,
0x9C00,
0x9D00,
0x9DA0,
0x9E40,
0x9EC0
])

zoomref = np.array(range(1,len(zoomtab)+1))

def zoomdata_model(zoomlevel, a,b,c,d):
	return a * (1 - (zoomlevel + c*zoomlevel**d)**b)

params = curve_fit(zoomdata_model, zoomref, zoomtab, [3e4, -0.5, 0.001,3])[0]

x = np.linspace(1,11)
y = zoomdata_model(x, *params)

print(params)

plt.figure(1)
plt.plot(zoomref, zoomtab, 'o', label='zoomtab')
plt.plot(x,y, label='zoom model')
plt.legend(loc='best')

plt.show(block=False)
#!/usr/bin/env python
import serial
import numpy 
from matplotlib import pyplot as plt

class baroAltitude:

	groundBaro = None
	airBaro = None
	str1 = None
	str2 = None
	groundData = 0.0
	airData = 0.0
	diffAlt = None
	altOffset = 0


	def openSerial(self):
		self.groundBaro = serial.Serial('/dev/ttyUSB0', 115200)
		self.airBaro = serial.Serial('/dev/ttyUSB1', 115200)

	def readBaro(self):
		self.str1 = self.groundBaro.readline().rstrip() # read data from serial 
		self.str2 = self.airBaro.readline().rstrip() # read data from serial

	def toFloat(self):
		split1 = self.str1.split(".")
		split2 = self.str2.split(".")
		try:
			self.groundData = float(split1[0]) + float(split1[1])/pow(10,len(split1[1]))
			self.airData = float(split2[0]) + float(split2[1])/pow(10,len(split2[1]))
		except:
			print "serial error"

	def getDiffAltitude(self):
		alt1 = (1-pow((self.groundData/101325),0.190284))*44307.69396
		#print "initial ground altitude (m) \t %f" % alt1
		alt2 = (1-pow((self.airData/101325),0.190284))*44307.69396
		#print "air data altitude (m) \t %f" %alt2
		self.diffAlt = abs(alt2 - alt1 - self.altOffset)
		#print self.diffAlt
		print self.groundData

alt = baroAltitude()

alt.openSerial()

# get first reading
while alt.groundData == 0:
	alt.readBaro()
	alt.toFloat()
	alt.getDiffAltitude()

# set the offset
alt.altOffset = alt.diffAlt
print "initial alt offset %f" %alt.altOffset

# get altitude difference
while True:
	alt.readBaro()
	alt.toFloat()
	alt.getDiffAltitude()
	#print alt.diffAlt - alt.altOffset

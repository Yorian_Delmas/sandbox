#include "ros/ros.h"
#include <cstdlib>
#include <rr_base_can/can.h>
#include <rr_base_msgs/GimbalControlRequest.h>


int main(int argc, char **argv)
{

  ros::init(argc, argv, "gimbal_pub");

  ros::NodeHandle n;


  ros::Publisher gimbal_pub = n.advertise<rr_base_msgs::GimbalControlRequest>("gimbalcontrolrequest", 1000);
  srand (time(NULL));

  ros::Rate loop_rate(0.1);

  ROS_INFO("starting loop\n");
  while (ros::ok())
  {

    rr_base_msgs::GimbalControlRequest msg;
    msg.pitch = rand() % 90;
    msg.yaw = rand() % 180;
    msg.zoom = rand() % 25600;
    msg.options = 11;
    gimbal_pub.publish(msg);
    ROS_INFO("message published");
    loop_rate.sleep();
  }
  return 0;
}
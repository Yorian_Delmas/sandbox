# README #

This README explains how to run the stress test on the camera-gimbal
### Notes ###
* Operating voltage is +12V. Do not exceed this value, and do not go under 11.3V (as camera won't start).
* UART1 can be connected on ICU for debug information. (115200 b/s)
* Please wire the entire setup before powering.
* If camera is powered after CCU, the firmware won't start.

### Wiring diagram ###
![wire_diagram.png](https://bitbucket.org/repo/RzEypA/images/1322699226-wire_diagram.png)
### Run the apalis nodes ###

* connect to apalis $ ssh root@<apalis-ip>
* source ~/setup.sh
* launch the canaerospace bridge $ roslaunch /opt/ros/indigo/share/rr_connect/launch/gimbal.launch
* launch the random setpoint node $ rosrun rr_gimbal_node gimbal_pub

### Who do I talk to? ###

* yorian.delmas@rapyuta-robotics.com